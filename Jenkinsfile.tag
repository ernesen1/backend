def PROJECT = "backend"
def BUILD_TAG = "$PROJECT"

currentBuild.displayName = "$PROJECT-#"+currentBuild.number

pipeline {
    
  agent any

  stages {  

    stage('Get tag') {
        environment {
          BUILD_TAG = sh(script: 'git tag | tail -1', returnStdout: true)
          TESTING = 'This is a test'
        }
        steps {
          sh '''
            echo "Get tag = $BUILD_TAG"
            echo "Get TESTING = $TESTING with $BUILD_TAG"
          '''
        }
    }
  }
}
